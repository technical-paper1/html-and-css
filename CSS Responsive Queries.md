# Responsive Queries using CSS

- CSS responsive queries, commonly known as media queries, allow you to apply different styles to your webpage based on the characteristics of the device or viewport. 
- They are a fundamental part of creating responsive web designs that adapt to various screen sizes and devices. Media queries are written in CSS and used in conjunction with the **@media** rule. 
  
## Media Query:

```css
@media screen and (max-width: 600px) {
    /*for screens with a maximum width of 600px */
}
```

#### Media Query Properties:

- screen: Specifies that the styles should apply to screens, but you can use other values like print, speech, or all.
- (max-width: 600px): The condition within the parentheses specifies when the rules should be applied. In this case, it's when the maximum viewport width is 600 pixels.

## Combining Conditions:
- You can combine multiple conditions using and, or, and parentheses to create complex queries.
```css
@media screen and (min-width: 600px) and (max-width: 1023px) {
    /* CSS rules for screens between 600px and 1023px */
}
```

## Orientational Queries:
- You can use orientation to target specific orientations, such as portrait or landscape.
```css
@media screen and (orientation:portrait)
{
    /* rule for portrait orientation */
}
```

### References

- [W3 Schools](https://www.w3schools.com/css/css_rwd_mediaqueries.asp)
- [Google](https://www.google.com)