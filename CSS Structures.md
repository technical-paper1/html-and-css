# CSS structural classes

- Structural pseudo classes allow access to the child elements present within the hierarchy of parent elements.
- We can select first-child element, last-child element, alternate elements present within the hierarchy of parent elements.

1. **:first-child**
:first-child represents the element that is first child element of the parent element.

2. **:nth-child(n)**
:nth-child(Expression) This represents the nth child element of the parent element based on the index of the child among the branch of child element.

1. **:last-child**
:last-child pseudo class represents the element that is at the end of its siblings in a tree structure.

1. **:nth-last-child(n)**
:nth-last-child(Expression) is the same as :nth-child(Expression) but the positioning of elements start from the end.


5. **:only-child**
:only-child represents the element that is a sole child of the parent element and there is no other sibling.

1. **:first-of-type**
There might be more than one type of siblings under a common parent. It selects the first element of the one type of sibling.

1. **:nth-of-type(n)**
There may be more than one elements of the same type under a common parent. :nth-of-type(Expression) represents those elements of the same type at the position evaluated by the Expression.

1. **:last-of-type**
:last-of-type represents the last element in the list of same type of siblings.

1. **:nth-last-of-type(n)**
It is the same as :nth-of-type(n) but it starts counting of position from the end instead of start.

### References

- [MDN Docs](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_is_structured)
- [Google](https://www.google.com)