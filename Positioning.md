# Positioning in CSS

- In web design and CSS, "positioning" refers to how an HTML element is placed and displayed within the layout of a web page. 
- Two common positioning properties are "position: relative" and "position: absolute," each with its own characteristics:

## Relative:

- When you set an element's position to "relative," it maintains its position within the normal flow of the document.
- You can then use properties like "top," "right," "bottom," and "left" to move the element relative to its normal position.
- Other elements in the document will still occupy space as if the relative element were in its original place.
- It's often used for fine-tuning the position of an element within its containing block.

## Absolute:

- When you set an element's position to "absolute," it's positioned relative to its nearest positioned ancestor, which means it's removed from the normal document flow.
- You can use "top," "right," "bottom," and "left" properties to precisely position the element within its containing block.
- Elements with "position: absolute" don't affect the layout of other elements, and other elements may overlap with them.
- It's commonly used for creating overlays, pop-up menus, or elements that need to be positioned precisely within a container.

<p align="center">
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBPd5oJDq3JjsZ14mhf3svjoivys8_LC69uA&usqp=CAU">
</p>

### References

- [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/CSS/position)
- [Freecodecamp](https://www.freecodecamp.org/news/css-positioning-position-absolute-and-relative/)