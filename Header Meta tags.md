# Header Meta Tags

- Meta tags in head block are used to provide extra information about the page or tell the browser on how to render the web page.

 A few meta tags that are crucial are :
  - **charset**:
    - Specifies the character encoding for the document, ensuring proper text rendering.
    ```html
    <meta charset="utf-8">
    ``` 
  - **name="viewport"**
    - It sets the viewport width to the device width and initial zoom level to 1.0.
    ```html
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    ```
  - **name="description"**
    -   Provides a concise description of the web page's content, often used by search engines.
    ```html
    <meta name="description" content="Brief description of the page">:
    ```   
  - **name="keywords"**
    - Lists keywords relevant to the page's content, which may help search engines understand the page's topic.
    ```html
    <meta name="keywords" content="keyword1, keyword2, keyword3">:
    ```
  - **name="author"**
    - Specifies the author of the page's content.
    ```html
    <meta name="author" content="Author's Name">:
    ``` 

### References

- [Meta Tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta)
- [W3 Schools](https://www.w3schools.com/tags/tag_meta.asp)



