# CSS Specificity

- CSS specificity is a concept that determines which CSS rule is applied when multiple rules target the same HTML element. 
- It's essential to understand specificity to avoid conflicts and ensure that the intended styles are applied. 
- Specificity is calculated based on the combination of selectors used in a CSS rule.
- Some of them are mentioned below : 

## Inline Styles: 
- Inline styles have the highest specificity. Styles applied directly within an HTML element using the style attribute override any other styles.

## ID Selectors: 
- ID selectors have a high specificity. They are denoted by # followed by an element's unique ID, e.g., #myElement.

## Class Selectors and Attribute Selectors: 
- Class selectors (e.g., .myClass) and attribute selectors (e.g., [type="text"]) have moderate specificity.

## Element Selectors and Pseudo-elements: 
- Element selectors (e.g., div, p) and pseudo-elements (e.g., ::before, ::after) have a lower specificity.

## Universal and Type Selectors: 
- Universal selectors (e.g., *) and type selectors (e.g., div, p) without any other qualifiers have the lowest specificity.

```css
*{
    margin:0%;
    /*global or universal selector*/
}
#myElement { color: red; } /*id selector*/
div.myClass { color: blue; } /*element and class selector*/

<div id="myElement" class="myClass">This is a div element.</div>

```

### References

- [GeeksforGeeks](https://www.geeksforgeeks.org/css-specificity/)
- [Web dev](https://web.dev/learn/css/specificity)