# Styling Classes in CSS

- CSS styling classes, often referred to as utility or helper classes, are classes designed to provide specific styling to elements without creating custom CSS rules for each case. 
- These classes help maintain a consistent and modular approach to styling elements on a web.
- Some of them are as follows:

## Text Styling:

- .text-center: Center-aligns text.
- .text-bold: Makes text bold.
- .text-italic: Applies italics to text.
- .text-uppercase: Converts text to uppercase.
- .text-lowercase: Converts text to lowercase.
## Background and Color:

- .bg-primary: Sets a background color for primary elements.
- .bg-secondary: Sets a background color for secondary elements.
- .text-primary: Sets the text color to the primary color.
- .text-secondary: Sets the text color to the secondary color.
## Spacing and Margins:

- .m-1, .m-2, ...: Adds margins (spacing) of different sizes.
- .p-1, .p-2, ...: Adds padding of different sizes.
- .m-auto: Centers an element horizontally with margin auto.
## Display and Visibility:

- .d-block: Sets the element to display as a block.
- .d-inline: Sets the element to display as inline.
- .hidden: Hides an element with display: none;

### References

- [W3 Schools](https://www.w3schools.com/cssref/sel_class.php)
- [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/CSS/Class_selectors)