# Flexbox and Grid

Flexbox and Grid are two CSS layout systems that provide powerful tools for creating responsive and complex web layouts. They each have their unique characteristics and use cases:

## Flexbox :

- Flexbox is designed for one-dimensional layouts, either as a row or a column.
- It's ideal for creating flexible and space-efficient layouts for components like navigation menus, toolbars, and items in a single row or column.
- Flexbox uses the display: flex; property for the parent container and flex properties for child elements (e.g., flex-grow, flex-shrink, flex-basis).
    ```css
    .container {
        display: flex;
        justify-content: space-between;
    }

    ```
## Grid :

- CSS Grid is designed for two-dimensional layouts, allowing you to create grid structures with rows and columns.
- Grid uses properties like grid-template-rows, grid-template-columns, and grid-area to define the grid layout.

    ```css
    .container {
        display: grid;
        grid-template-columns: 1fr 2fr 1fr;
        grid-template-areas:
        "a b"
        "c c";
        /*here in the first row we placed 2 items and a single item in the 2nd row*/
        grid-template-rows: 100px 200px;
    }
    ```

### References

- [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_grid_layout/Relationship_of_grid_layout_with_other_layout_methods)
- [Google](https://www.google.com)